import 'package:cloud_firestore/cloud_firestore.dart';

class Post {
  final String id;
  final String imageUrl;
  final String caption;
  final String authorId;
  final Timestamp timestamp;
  final int likesCount;

  Post(
      {this.id,
      this.imageUrl,
      this.caption,
      this.likesCount,
      this.authorId,
      this.timestamp});

  factory Post.fromDoc(DocumentSnapshot doc) {
    return Post(
        id: doc.documentID,
        caption: doc['caption'],
        imageUrl: doc['imageUrl'],
        authorId: doc['authorId'],
        likesCount: doc['likesCount'],
        timestamp: doc['timestamp']);
  }
}
