import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:picster/models/post_model.dart';
import 'package:picster/models/user_model.dart';
import 'package:picster/screens/profile_screen.dart';
import 'package:picster/services/auth_service.dart';
import 'package:picster/services/database_service.dart';
import 'package:picster/widgets/post_view.dart';

class FeedScreen extends StatefulWidget {
  static final String id = 'feed_screen';

  final String currentUserId;

  FeedScreen({this.currentUserId});
  @override
  _FeedScreenState createState() => _FeedScreenState();
}

class _FeedScreenState extends State<FeedScreen> {
  List<Post> _posts = [];

  @override
  void initState() {
    super.initState();
    _setupFeed();
  }

  _setupFeed() async {
    List<Post> posts = await DatabaseService.getFeedPosts(widget.currentUserId);
    setState(() {
      _posts = posts;
    });
  }

  _buildPost(Post post, User author) {
    return PostView(
      currentUserId: widget.currentUserId,
      author: author,
      post: post,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        actions: <Widget>[
          IconButton(
            onPressed: () {
              AuthService.logout(context);
            },
            icon: Icon(
              Icons.settings_power,
              color: Colors.black,
            ),
          )
        ],
        title: Center(
          child: Text(
            'Picster',
            style: TextStyle(
              color: Colors.black,
              fontSize: 35.0,
              fontFamily: 'Billabong',
            ),
          ),
        ),
      ),
      body: _posts.length > 0
          ? RefreshIndicator(
              onRefresh: () => _setupFeed(),
              child: ListView.builder(
                  itemCount: _posts.length,
                  itemBuilder: (BuildContext context, int index) {
                    Post post = _posts[index];
                    return FutureBuilder(
                        future: DatabaseService.getUserWithId(post.authorId),
                        builder:
                            (BuildContext context, AsyncSnapshot snapshot) {
                          if (!snapshot.hasData) {
                            return SizedBox.shrink();
                          }
                          User author = snapshot.data;
                          return _buildPost(post, author);
                        });
                  }),
            )
          : Center(
              child: CircularProgressIndicator(),
            ),
    );
  }
}
