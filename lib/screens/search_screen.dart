import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:picster/models/user_data.dart';
import 'package:picster/models/user_model.dart';
import 'package:picster/screens/profile_screen.dart';
import 'package:picster/services/database_service.dart';
import 'package:provider/provider.dart';

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  TextEditingController _searchController = TextEditingController();

  _buildUserTile(User user) {
    return ListTile(
      leading: CircleAvatar(
        radius: 20,
        backgroundColor: Colors.grey,
        backgroundImage: user.profileImageUrl.isEmpty
            ? AssetImage('assets/images/placeholder.png')
            : CachedNetworkImageProvider(user.profileImageUrl),
      ),
      title: Text(user.name),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (_) => ProfileScreen(
                  currentUserId: Provider.of<UserData>(context).currentUserId,
                  userId: user.id)),
        );
      },
    );
  }

  _clearSearch() {
    WidgetsBinding.instance
        .addPostFrameCallback((_) => _searchController.clear());
    setState(() {
      _users = null;
    });
  }

  Future<QuerySnapshot> _users;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: TextField(
            controller: _searchController,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.symmetric(vertical: 15),
              border: InputBorder.none,
              hintText: 'Search',
              prefixIcon: Icon(
                Icons.search,
                size: 30,
                color: Colors.black45,
              ),
              suffixIcon: IconButton(
                onPressed: () {
                  _clearSearch();
                },
                icon: Icon(
                  Icons.clear,
                  size: 25,
                  color: Colors.black45,
                ),
              ),
              filled: true,
            ),
            onSubmitted: (input) {
              if (input.isNotEmpty) {
                setState(() {
                  _users = DatabaseService.searchUsers(input);
                });
              }
            },
          ),
        ),
        body: _users == null
            ? Center(
                child: Text('Search For User'),
              )
            : FutureBuilder(
                future: _users,
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  if (!snapshot.hasData) {
                    return Center(child: CircularProgressIndicator());
                  }
                  if (snapshot.data.documents.length == 0) {
                    return Center(
                      child: Text('No Users Found!'),
                    );
                  }
                  return ListView.builder(
                      itemCount: snapshot.data.documents.length,
                      itemBuilder: (BuildContext context, int index) {
                        User user =
                            User.fromDoc(snapshot.data.documents[index]);
                        return _buildUserTile(user);
                      });
                }));
  }
}
