import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:picster/models/post_model.dart';
import 'package:picster/models/user_data.dart';
import 'package:picster/models/user_model.dart';
import 'package:picster/screens/edit_profile_screen.dart';
import 'package:picster/screens/single_post_screen.dart';
import 'package:picster/services/database_service.dart';

import 'package:picster/utilities/constants.dart';
import 'package:picster/widgets/post_view.dart';
import 'package:provider/provider.dart';

class ProfileScreen extends StatefulWidget {
  final String userId;
  final String currentUserId;

  ProfileScreen({this.currentUserId, this.userId});
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  bool _isFollowing = false;
  int _followersCount;
  int _followingCount;
  List<Post> _posts = [];
  int _displayPosts = 0; // 0-grid
  User _profileUser;

  _displayButton(User user) {
    return user.id == Provider.of<UserData>(context).currentUserId
        ? FlatButton(
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (_) => EditProfileScreen(user: user)));
            },
            textColor: Colors.white,
            color: Colors.blue,
            child: Text(
              'Edit Profile',
              style: TextStyle(fontSize: 18),
            ),
          )
        : FlatButton(
            onPressed: _followOrUnFollow,
            textColor: _isFollowing ? Colors.black : Colors.white,
            color: _isFollowing ? Colors.grey[200] : Colors.blue,
            child: Text(
              _isFollowing ? 'Unfollow' : 'Follow',
              style: TextStyle(fontSize: 18),
            ),
          );
  }

  _followOrUnFollow() {
    if (_isFollowing) {
      _unFollowUser();
    } else {
      _followUser();
    }
  }

  _unFollowUser() {
    DatabaseService.unFollowUser(
        currentUserId: widget.currentUserId, userId: widget.userId);
    setState(() {
      _isFollowing = false;
      _followersCount--;
    });
  }

  _followUser() {
    DatabaseService.followUser(
        currentUserId: widget.currentUserId, userId: widget.userId);
    setState(() {
      _isFollowing = true;
      _followersCount++;
    });
  }

  @override
  void initState() {
    super.initState();
    _setUpIsFollowing();
    _setUpFollowers();
    _setUpFollowing();
    _setUpPosts();
    _setupProfileUser();
  }

  _setupProfileUser() async {
    User profileUser = await DatabaseService.getUserWithId(widget.userId);
    setState(() {
      _profileUser = profileUser;
    });
  }

  _setUpIsFollowing() async {
    bool isFollowingUser = await DatabaseService.isFollowingUser(
        currentUserId: widget.currentUserId, userId: widget.userId);
    print(isFollowingUser);
    setState(() {
      _isFollowing = isFollowingUser;
    });
  }

  _setUpFollowers() async {
    int followers = await DatabaseService.numFollowers(userId: widget.userId);
    setState(() {
      _followersCount = followers;
    });
  }

  _setUpFollowing() async {
    int following = await DatabaseService.numFollowing(userId: widget.userId);
    setState(() {
      _followingCount = following;
    });
  }

  _setUpPosts() async {
    List<Post> posts = await DatabaseService.getUserPosts(widget.userId);
    setState(() {
      _posts = posts;
    });
  }

  _buildProfileInfo(User user) {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
          child: Row(
            children: <Widget>[
              CircleAvatar(
                radius: 50,
                backgroundColor: Colors.grey,
                backgroundImage: user.profileImageUrl.isEmpty
                    ? AssetImage('assets/images/placeholder.png')
                    : CachedNetworkImageProvider(user.profileImageUrl),
              ),
              Expanded(
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Column(
                          children: <Widget>[
                            Text(
                              _posts.length.toString(),
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.w600),
                            ),
                            Text(
                              'Posts',
                              style: TextStyle(color: Colors.black54),
                            )
                          ],
                        ),
                        Column(
                          children: <Widget>[
                            Text(
                              _followersCount.toString(),
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.w600),
                            ),
                            Text(
                              'Followers',
                              style: TextStyle(color: Colors.black54),
                            )
                          ],
                        ),
                        Column(
                          children: <Widget>[
                            Text(
                              _followingCount.toString(),
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.w600),
                            ),
                            Text(
                              'Following',
                              style: TextStyle(color: Colors.black54),
                            )
                          ],
                        ),
                      ],
                    ),
                    Container(
                      width: 200,
                      child: _displayButton(user),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                user.name,
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
              ),
              SizedBox(
                height: 5,
              ),
              Container(
                height: 80,
                child: Text(
                  user.bio,
                  style: TextStyle(fontSize: 15),
                ),
              ),
              Divider(),
            ],
          ),
        ),
      ],
    );
  }

  _buildToggleButtons() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.grid_on,
            size: 30,
            color: _displayPosts == 0
                ? Theme.of(context).primaryColor
                : Colors.grey,
          ),
          onPressed: () {
            setState(() {
              _displayPosts = 0;
            });
          },
        ),
        IconButton(
          icon: Icon(
            Icons.list,
            size: 30,
            color: _displayPosts == 1
                ? Theme.of(context).primaryColor
                : Colors.grey,
          ),
          onPressed: () {
            setState(() {
              _displayPosts = 1;
            });
          },
        )
      ],
    );
  }

  _buildTilePosts(Post post) {
    return GridTile(
      child: GestureDetector(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (_) => SinglePostScreen(
                        post: post,
                        currentUserId: widget.currentUserId,
                        authorId: post.authorId,
                      )));
        },
        child: Image(
          image: CachedNetworkImageProvider(post.imageUrl),
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  _buildDisplayPosts() {
    if (_displayPosts == 0) {
      List<GridTile> tiles = [];
      _posts.forEach((post) {
        tiles.add(_buildTilePosts(post));
      });
      return GridView.count(
        crossAxisCount: 3,
        childAspectRatio: 1,
        mainAxisSpacing: 2,
        crossAxisSpacing: 2,
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        children: tiles,
      );
    } else {
      List<PostView> postViews = [];
      _posts.forEach((post) {
        postViews.add(PostView(
          currentUserId: widget.currentUserId,
          post: post,
          author: _profileUser,
        ));
      });
      return Column(children: postViews);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Center(
          child: Text(
            'Picster',
            style: TextStyle(
              color: Colors.black,
              fontSize: 35.0,
              fontFamily: 'Billabong',
            ),
          ),
        ),
      ),
      backgroundColor: Colors.white,
      body: FutureBuilder(
        future: usersRef.document(widget.userId).get(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.hasData) {
            User user = User.fromDoc(snapshot.data);
            return ListView(
              children: <Widget>[
                _buildProfileInfo(user),
                _buildToggleButtons(),
                Divider(),
                _buildDisplayPosts()
              ],
            );
          }

          return Center(
            child: CircularProgressIndicator(
              backgroundColor: Colors.white,
              valueColor: new AlwaysStoppedAnimation<Color>(Color(0xff000000)),
            ),
          );
        },
      ),
    );
  }
}
