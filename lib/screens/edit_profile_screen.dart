import 'dart:io';
import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:picster/models/user_model.dart';
import 'package:picster/services/database_service.dart';
import 'package:picster/services/storage_service.dart';

class EditProfileScreen extends StatefulWidget {
  final User user;

  EditProfileScreen({this.user});
  @override
  _EditProfileScreenState createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<EditProfileScreen> {
  bool _isLoading = false;

  File _profileImage;
  final _formKey = GlobalKey<FormState>();
  String _name = '';
  String _bio = '';

  _handleImageFromGallery() async {
    File imageFile = await ImagePicker.pickImage(source: ImageSource.gallery);
    if (imageFile != null) {
      setState(() {
        _profileImage = imageFile;
      });
    }
  }

  _displayProfileImage() {
    if (_profileImage == null) {
      if (widget.user.profileImageUrl.isEmpty) {
        return AssetImage('assets/images/placeholder.png');
      } else {
        return CachedNetworkImageProvider(widget.user.profileImageUrl);
      }
    } else {
      return FileImage(_profileImage);
    }
  }

  _submit() async {
    setState(() {
      _isLoading = true;
    });
    if (_formKey.currentState.validate() && !_isLoading) {
      _formKey.currentState.save();
      String _profileImageUrl = '';

      if (_profileImageUrl == null) {
        _profileImageUrl = widget.user.profileImageUrl;
      } else {
        _profileImageUrl = await StorageService.uploadUserProfileImage(
            widget.user.profileImageUrl, _profileImage);
      }

      User user = User(
        id: widget.user.id,
        profileImageUrl: _profileImageUrl,
        bio: _bio,
        name: _name,
      );

      DatabaseService.updateUser(user);
      Navigator.pop(context);
    }
  }

  @override
  void initState() {
    super.initState();
    _name = widget.user.name;
    _bio = widget.user.bio;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          'Edit Profile',
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: ListView(
          children: <Widget>[
            _isLoading
                ? LinearProgressIndicator(
                    backgroundColor: Colors.blue[200],
                    valueColor: AlwaysStoppedAnimation(Colors.blue),
                  )
                : SizedBox.shrink(),
            Padding(
              padding: const EdgeInsets.all(30.0),
              child: Form(
                key: _formKey,
                child: Column(
                  children: <Widget>[
                    CircleAvatar(
                      radius: 50,
                      backgroundColor: Colors.grey,
                      backgroundImage: _displayProfileImage(),
                    ),
                    FlatButton(
                        onPressed: _handleImageFromGallery,
                        child: Text(
                          'Change Profile Image',
                          style: TextStyle(
                              color: Theme.of(context).accentColor,
                              fontSize: 15),
                        )),
                    TextFormField(
                      initialValue: _name,
                      style: TextStyle(fontSize: 18),
                      decoration: InputDecoration(
                        icon: Icon(
                          Icons.person,
                          size: 20,
                        ),
                        labelText: 'Name',
                      ),
                      validator: (input) => input.trim().length < 1
                          ? 'Name cannot be empty'
                          : null,
                      onSaved: (input) {
                        _name = input;
                      },
                    ),
                    TextFormField(
                      maxLines: 4,
                      initialValue: _bio,
                      style: TextStyle(fontSize: 18),
                      decoration: InputDecoration(
                        icon: Icon(
                          Icons.book,
                          size: 20,
                        ),
                        labelText: 'Bio',
                      ),
                      validator: (input) => input.trim().length > 150
                          ? 'Bio cannot be more than 150 characters'
                          : null,
                      onSaved: (input) {
                        _bio = input;
                      },
                    ),
                    Container(
                      margin: EdgeInsets.all(40),
                      height: 40,
                      width: 250,
                      child: FlatButton(
                        textColor: Colors.white,
                        color: Colors.blue,
                        onPressed: _submit,
                        child: Text(
                          'Save Profile',
                          style: TextStyle(fontSize: 18),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
