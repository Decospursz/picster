import 'package:flutter/material.dart';
import 'package:picster/models/post_model.dart';
import 'package:picster/models/user_model.dart';
import 'package:picster/services/database_service.dart';
import 'package:picster/widgets/post_view.dart';

class SinglePostScreen extends StatefulWidget {
  final String currentUserId;
  final Post post;
  final String authorId;

  const SinglePostScreen(
      {Key key, this.currentUserId, this.post, this.authorId})
      : super(key: key);
  @override
  _SinglePostScreenState createState() => _SinglePostScreenState();
}

class _SinglePostScreenState extends State<SinglePostScreen> {
  User _author;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _setupAuthor();
  }

  _setupAuthor() async {
    User author = await DatabaseService.getUserWithId(widget.authorId);
    if (mounted) {
      setState(() {
        _author = author;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: Text(
            'Post',
            style: TextStyle(color: Colors.black),
          ),
        ),
        body: _author != null
            ? ListView(
                children: <Widget>[
                  PostView(
                    currentUserId: widget.currentUserId,
                    author: _author,
                    post: widget.post,
                  ),
                ],
              )
            : Center(child: CircularProgressIndicator()));
  }
}
